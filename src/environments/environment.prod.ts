export const environment = {
  production: true,
  appSubdir: '/Register/',
  auth: {
    clientID: 'png_angular',
    issuer: 'https://pau-training.pcgacademia.pl/idp.server',
    scope: 'openid profile email roles u10-server-dziekanat'
  },
  apiUrl: 'https://pau-training.pcgacademia.pl/PNGU10API/api'
};
