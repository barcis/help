export const environment = {
  production: false,
  appSubdir: '/',
  auth: {
    clientID: 'png_angular',
    issuer: 'https://pau-training.pcgacademia.pl/idp.server',
    scope: 'openid profile email roles u10-server-dziekanat identityserverapi',
    apiUrl: 'http://pau-training.pcgacademia.pl/IDP.api/api'
  },
  apiUrl: 'https://localhost:5001/api'
};
