export interface FailedAccessAttempt {
  created: Date;
  clientId: string;
  ip: string;
  id: number;
}
