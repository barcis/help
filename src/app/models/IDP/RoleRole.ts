export interface RoleRole {
  description: string;
  name: string;
  id: number;
}
