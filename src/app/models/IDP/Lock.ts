export interface Lock {
  description: string;
  expiration: Date;
  type: string;
  isActive?: boolean;
  created: Date;
  id: number;
  value?: string;
}
