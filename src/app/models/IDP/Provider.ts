export interface Provider {
  providerId: number;
  login: string;
  id: number;
}
