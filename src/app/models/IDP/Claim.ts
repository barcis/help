export interface Claim {
  type: string;
  value: string;
  id: number;
}
