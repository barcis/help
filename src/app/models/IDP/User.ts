import { Claim } from './Claim';
import { RoleElement } from './RoleElement';
import { Lock } from './Lock';
import { Provider } from './Provider';
import { FailedAccessAttempt } from './FailedAccessAttempt';

export class User {
  subjectId: string;
  isActive: boolean;
  login: string;
  created: Date;
  claims: Claim[];
  locks: Lock[];
  providers: Provider[];
  secrets: Lock[];
  roles: RoleElement[];
  failedAccessAttempts: FailedAccessAttempt[];
}


