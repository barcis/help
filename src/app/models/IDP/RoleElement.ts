import { RoleRole } from './RoleRole';
export interface RoleElement {
  role: RoleRole;
  id: number;
}
