export class StatusName {
  id: number;
  name: string;
  active: boolean;
}
