import { EnrollmentStatus } from './EnrollmentStatus';
import { Type } from 'class-transformer';
export class StudentEnrollmentStatus {
  id: number;
  @Type(() => EnrollmentStatus)
  value: EnrollmentStatus;
}
