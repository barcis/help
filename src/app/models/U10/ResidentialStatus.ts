export class ResidentialStatus {
  id: number;
  name: string;
  active: boolean;
}
