export class Specialization {
  id: number;
  name: string;
  short: string;
  code: string;
}
