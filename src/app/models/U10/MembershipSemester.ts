import { Type } from 'class-transformer';
import { Semester } from './Semester';
import { StudentEnrollmentStatus } from './StudentEnrollmentStatus';
export class MembershipSemester {
  id: number;
  year: number;
  manualAverage: number;

  @Type(() => Semester)
  semester: Semester;

  @Type(() => StudentEnrollmentStatus)
  enrollmentStatus: StudentEnrollmentStatus;
}
