import { Type } from 'class-transformer';
import { AdditionalDictionary } from './AdditionalDictionary';

export class AdditionalType {
  id: number;
  name: string;
  description: string;
  type: number;
  edit: number;

  @Type(() => AdditionalDictionary)
  options: Array<AdditionalDictionary>;
}


