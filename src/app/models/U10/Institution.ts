import { Department } from './Department';
import { Type } from 'class-transformer';

export class Institution {
  id: number;
  name: string;
  institutionId: number;
  @Type(() => Department)
  departments: Array<Department>;
}
