export class ScholarshipStatus {
  id: number;
  name: string;
  active: boolean;
}
