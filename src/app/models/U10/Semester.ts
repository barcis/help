export class Semester {
  name: string;
  number: number;
  get semesterOfYear(): number {
    return 2 - (this.number % 2);
  }
  get yearOfStudy() {
    return Math.ceil(this.number / 2);
  }
}
