import { Type } from 'class-transformer';
import { Citizenship } from './Citizenship';

export class Person {
  id: number;
  surname: string;
  name: string;
  gender: number;
  subId: string;
  email: string;
  email2: string;
  emailWork: string;
  phoneNumber: string;
  phoneNumberDormitory: string;
  phoneNumberHome: string;
  phoneNumberHome2: string;
  phoneNumberWork: string;
  phoneNumberWork2: string;
  phoneNumberOther: string;
  @Type(() => Date)
  dateOfBirth: Date;
  placeOfBirth: string;
  @Type(() => Citizenship)
  citizenships: Array<Citizenship>;
  nationality: string;
  origin: string;
  company: string;
  maritalStatus: number;
  get genderAsLetter(): string {
    return this.gender === 1 ? 'F' : 'M';
  }
  get genderAsText(): string {
    return this.gender === 1 ? 'Female' : 'Male';
  }
}
