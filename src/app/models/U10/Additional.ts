import { Type } from 'class-transformer';
import { AdditionalType } from './AdditionalType';
export class Additional {
  id: number;
  value: number | string | Date;
  studentId: number;

  @Type(() => AdditionalType)
  type: AdditionalType;
}
