export class Mode {
  id: number;
  name: string;
  code: string;
  short: string;
}
