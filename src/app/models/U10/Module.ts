import { Type } from 'class-transformer';
import { Specialization } from './Specialization';
import { Specialty } from './Specialty';
export class Module {
  id: number;
  @Type(() => Specialization)
  specialization: Specialization;
  @Type(() => Specialty)
  specialty: Specialty;
}
