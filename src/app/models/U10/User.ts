export class User {
  id: number;
  surname: string;
  name: string;
  email: string;
  login: string;
  role: string;
  subId: string;
  personId: number;
  token?: string;
}
