import { Type } from 'class-transformer';
import { MembershipSemester } from './MembershipSemester';
import { Department } from './Department';
import { Programme } from './Programme';
import { Institution } from './Institution';
import { Kind } from './Kind';
import { Module } from './Module';

export class Membership {
  id: number;
  gpa: number;
  albumNumber: string;
  additional: Object = {};

  @Type(() => Date)
  startDate: Date;

  @Type(() => Date)
  universityStartDate: Date;

  @Type(() => Department)
  department: Department;

  @Type(() => Institution)
  institution: Institution;

  @Type(() => Programme)
  programme: Programme;

  @Type(() => Kind)
  kind: Kind;

  @Type(() => Module)
  module: Module;

  @Type(() => MembershipSemester)
  membershipSemester: MembershipSemester;
}
