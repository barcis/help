import { Type } from 'class-transformer';
import { Mode } from './Mode';
export class Kind {
  id: number;
  name: string;
  code: string;
  short: string;
  @Type(() => Mode)
  mode: Mode;
}
