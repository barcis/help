export class AdditionalDictionary {
  id: number;
  ordinalNumber: number;
  value: string;
  short: string;
}
