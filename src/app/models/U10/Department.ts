import { Type } from 'class-transformer';
import { Programme } from './Programme';
export class Department {
  id: number;
  name: string;
  @Type(() => Programme)
  programmes: Array<Programme>;
}
