export class Programme {
  id: number;
  name: string;
  code: string;
  short: string;
}
