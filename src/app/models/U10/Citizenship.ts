export class Citizenship {
  id: number;
  value: string;
  short: string;
}
