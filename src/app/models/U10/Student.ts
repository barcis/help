import { Person } from './Person';
import { Membership } from './Membership';
import { Type } from 'class-transformer';

export class Student {
  private _membership = 0;
  id: number;

  @Type(() => Person)
  person: Person;
  uni10Id: string;
  @Type(() => Membership)
  memberships: Array<Membership>;
  allocator: string;
  portOfTravel: string;
  enrollmentStatus: string;
  residentialStatus: string;
  scholarshipStatus: string;
  comment: string | undefined;
  additional: Object = {};
  get hasComment(): boolean {
    return this.comment && this.comment.length > 0;
  }
  get commentBrief(): string {
    if (!this.comment) {
      return '';
    } else if (this.comment.length <= 150) {
      return this.comment;
    } else {
      return this.comment.substr(0, 147) + '...';
    }
  }
  set membershipIndex(index: number) {
    this._membership = index;
  }
  get membershipIndex(): number {
    return this._membership;
  }
  get membership(): Membership {
    return this.memberships[this._membership];
  }
}
