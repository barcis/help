export class Specialty {
  id: number;
  name: string;
  short: string;
  code: string;
}
