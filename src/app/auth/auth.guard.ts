import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad, Route } from '@angular/router';
import { OidcSecurityService } from 'angular-auth-oidc-client';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private oidcSecurityService: OidcSecurityService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.oidcSecurityService.getUserData().pipe<boolean>(
      map(userData => {
        if (!userData) {
          console.log('user not loggedin');
          return false;
        }

        if (!route.data.expectedRole) {
          console.log(`role not expected`);
          return true;
        }

        const roles: Array<string> = !Array.isArray(userData.role) ? [userData.role] : userData.role;

        if (roles.indexOf(route.data.expectedRole) >= 0) {
          console.log(`user have expected role: '${route.data.expectedRole}'`, roles);
          return true;
        }
        console.log(`user don't have role: '${route.data.expectedRole}'`);
        return false;
      }));
  }
}
