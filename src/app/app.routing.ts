import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';

const appRoutes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./modules/base/base.module').then(
        m => m.BaseModule
      )
  },
  {
    path: 'register',
    loadChildren: () =>
      import('./modules/register/register.module').then(
        m => m.RegisterModule
      ),
    canActivate: [AuthGuard],
    data: {
      expectedRole: 'Supervisor'
    }
  },
  {
    path: 'user',
    loadChildren: () =>
      import('./modules/user/user.module').then(
        m => m.UserModule
      ),
    canActivate: [AuthGuard],
    data: {
      expectedRole: 'idpadmin'
    }
  },
  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

export const AppRouting = RouterModule.forRoot(appRoutes);
