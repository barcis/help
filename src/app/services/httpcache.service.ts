import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class HttpCacheService {
  private datas: any = {};
  constructor() { }
  put(url: string, data: any): void {
    this.datas[url] = data;
  }
  get(url: string): any {
    return this.datas[url];
  }
  invalidateCache(): void {
    this.datas = {};
  }
}
