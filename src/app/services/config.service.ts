import { DetalsDefinitionFieldXML } from './../modules/register/class/DetalsDefinitionField';
import { DetalsDefinitionGroupXML } from './../modules/register/class/DetalsDefinitionGroup';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DetalsDefinitionXML } from '../modules/register/class/DetalsDefinition';
import { map } from 'rxjs/operators';
import { Parser } from 'xml2js';
import { DetalsDefinitionTabXML } from '../modules/register/class/DetalsDefinitionTab';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  constructor(private http: HttpClient) { }
  getConfigJSON() {
    return this.http.get<DetalsDefinitionXML>(`/assets/config.json`);
  }

  getConfigXML() {
    return this.http.get('/assets/config.xml',
      {
        headers: new HttpHeaders()
          .set('Content-Type', 'text/xml')
          .append('Access-Control-Allow-Methods', 'GET')
          .append('Access-Control-Allow-Origin', '*')
          .append(
            'Access-Control-Allow-Headers',
            'Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Request-Method'),
        responseType: 'text'
      }).pipe(
        map(data => this.parseXML(data))
      );
  }

  private parseXML(data): Promise<DetalsDefinitionXML> {
    return new Promise(resolve => {
      const ddXML = Object.assign(new DetalsDefinitionXML(), { tabs: [] });
      const parser = new Parser({ trim: true, explicitArray: false });

      parser.parseString(data, function (err, result) {
        if (result.config && result.config.details && result.config.details.tab) {
          if (!Array.isArray(result.config.details.tab)) {
            result.config.details.tab = [result.config.details.tab];
          }
          for (const _tab of result.config.details.tab) {
            const tab = Object.assign(new DetalsDefinitionTabXML(), { groups: [] }, _tab.$);
            if (_tab.group) {
              if (!Array.isArray(_tab.group)) {
                _tab.group = [_tab.group];
              }
              for (const _group of _tab.group) {
                const group = Object.assign(new DetalsDefinitionGroupXML(), { fields: [] }, _group.$);
                if (_group.field) {
                  if (!Array.isArray(_group.field)) {
                    _group.field = [_group.field];
                  }

                  for (const _field of _group.field) {
                    const field = Object.assign(new DetalsDefinitionFieldXML(), {}, _field.$);
                    group.fields.push(field);
                  }
                }
                tab.groups.push(group);
              }
            }
            ddXML.tabs.push(tab);
          }
        }
        resolve(ddXML);
      });
    });
  }
}
