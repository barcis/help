import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/IDP/User';
import { environment } from 'src/environments/environment';
import { plainToClass } from 'class-transformer';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  createOne(name: string, surname: string, email: string, login: string): Observable<User> {
    return this.http
      .post<User>(`${environment.auth.apiUrl}/Users`, { givenName: name, familyName: surname, email, login })
      .pipe(map(e => plainToClass(User, e)));
  }

  setPassword(id: string, value: string) {
    return this.http
      .post<User>(`${environment.auth.apiUrl}/Users/${id}/secrets`, { value, description: 'Created by Registration Platform script' })
      .pipe(map(e => plainToClass(User, e)));
  }
  addRole(id: string, roleId: number) {
    return this.http
      .post<User>(`${environment.auth.apiUrl}/Users/${id}/roles?roleId=${roleId}`, {})
      .pipe(map(e => plainToClass(User, e)));
  }
}
