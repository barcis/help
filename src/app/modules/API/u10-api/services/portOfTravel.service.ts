import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { PortOfTravel } from 'src/app/models/U10/PortOfTravel';
import { HttpCacheService } from 'src/app/services/httpcache.service';
@Injectable({
  providedIn: 'root'
})
export class PortOfTravelService {
  constructor(private http: HttpClient, private cache: HttpCacheService) { }
  getAll(): Observable<PortOfTravel[]> {
    const params = new HttpParams()
      .set('pagination.PageNumber', '0')
      .set('pagination.PageSize', '100');


    const cacheId = '/PortsOfTravel?' + params.toString();
    const _data = this.cache.get(cacheId);

    if (_data) {
      return of(_data);
    }

    return this.http
      .get<PortOfTravel[]>(`${environment.apiUrl}/PortsOfTravel`, {
        params
      })
      .pipe(
        map(a => {
          const data$ = a.map(e => Object.assign(new PortOfTravel(), e));
          this.cache.put(cacheId, data$);
          return data$;
        })
      );
  }
}

