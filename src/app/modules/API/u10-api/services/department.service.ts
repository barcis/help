import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Department } from 'src/app/models/U10/Department';
import { HttpCacheService } from 'src/app/services/httpcache.service';
@Injectable({
  providedIn: 'root'
})
export class DepartmentService {
  constructor(private http: HttpClient, private cache: HttpCacheService) { }

  getAll(institutions: number[]): Observable<Department[]> {
    let params = new HttpParams()
      .set('pagination.PageNumber', '0')
      .set('pagination.PageSize', '100');


    institutions.forEach(id => {
      params = params.append('institutions', id.toString());
    });
    const cacheId = '/Departments?' + params.toString();

    const _data = this.cache.get(cacheId);

    if (_data) {
      return of(_data);
    }

    const observable$ = this.http
      .get<Department[]>(`${environment.apiUrl}/Departments`, {
        params
      })
      .pipe(
        map(a => {
          const data$ = a.map(e => Object.assign(new Department(), e));
          this.cache.put(cacheId, data$);
          return data$;
        })
      );

    return observable$;
  }

  // getAll(institutions: number[]): Observable<Department[]> {
  //   let params = new HttpParams()
  //     .set('pagination.PageNumber', '0')
  //     .set('pagination.PageSize', '100');
  //   institutions.forEach(id => {
  //     params = params.append('institutions', id.toString());
  //   });
  //   return this.http
  //     .get<Department[]>(`${environment.apiUrl}/Departments`, {
  //       params
  //     })
  //     .pipe(map(a => a.map(e => Object.assign(new Department(), e))));
  // }
  getOne(id: number): Observable<Department> {
    return this.http
      .get<Department>(`${environment.apiUrl}/Departments/${id}`)
      .pipe(map(e => Object.assign(new Department(), e)));
  }
}
