import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { EnrollmentStatus } from 'src/app/models/U10/EnrollmentStatus';
import { HttpCacheService } from 'src/app/services/httpcache.service';
@Injectable({
  providedIn: 'root'
})
export class EnrollmentStatusService {
  constructor(private http: HttpClient, private cache: HttpCacheService) { }
  getAll(): Observable<EnrollmentStatus[]> {
    const params = new HttpParams()
      .set('pagination.PageNumber', '0')
      .set('pagination.PageSize', '100');

    const cacheId = '/EnrollmentStatuses?' + params.toString();
    const _data = this.cache.get(cacheId);

    if (_data) {
      return of(_data);
    }

    const observable$ = this.http
      .get<EnrollmentStatus[]>(`${environment.apiUrl}/EnrollmentStatuses`, {
        params
      })
      .pipe(map(a => {
        const data$ = a.map(e => Object.assign(new EnrollmentStatus(), e));
        this.cache.put(cacheId, data$);
        return data$;
      }
      ));

    return observable$;
  }
}
