import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Institution } from 'src/app/models/U10/Institution';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpCacheService } from 'src/app/services/httpcache.service';

@Injectable({
  providedIn: 'root'
})
export class InstitutionService {
  constructor(private http: HttpClient, private cache: HttpCacheService) { }

  getAll(): Observable<Institution[]> {
    const params = new HttpParams()
      .set('pagination.PageNumber', '0')
      .set('pagination.PageSize', '100');

    const cacheId = '/Institutions?' + params.toString();
    const _data = this.cache.get(cacheId);

    if (_data) {
      return of(_data);
    }

    const observable$ = this.http
      .get<Institution[]>(`${environment.apiUrl}/Institutions`, {
        params
      })
      .pipe(
        map(a => {
          const data$ = a.map(e => Object.assign(new Institution(), e));
          this.cache.put(cacheId, data$);
          return data$;
        })
      );

    return observable$;
  }

  getOne(id: number): Observable<Institution> {
    return this.http
      .get<Institution>(`${environment.apiUrl}/Institutions/${id}`)
      .pipe(map(e => Object.assign(new Institution(), e)));
  }
}
