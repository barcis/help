import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Additional } from 'src/app/models/U10/Additional';
@Injectable({
  providedIn: 'root'
})
export class AdditionalService {
  constructor(private http: HttpClient) { }
  getOneForStudent(studentId: number, typeId: number) {
    return this.http.get<Additional>(`${environment.apiUrl}/Additional/${typeId}/Student/${studentId}`);
  }
}
