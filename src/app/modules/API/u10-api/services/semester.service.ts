import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MembershipSemester } from 'src/app/models/U10/MembershipSemester';
@Injectable({
  providedIn: 'root'
})
export class SemesterService {
  constructor(private http: HttpClient) { }
  getAll(
    institutions: number[],
    departments: number[],
    programmes: number[]
  ): Observable<MembershipSemester[]> {
    let params = new HttpParams()
      .set('pagination.PageNumber', '0')
      .set('pagination.PageSize', '100');
    institutions.forEach(id => {
      params = params.append('institutions', id.toString());
    });
    departments.forEach(id => {
      params = params.append('departments', id.toString());
    });
    programmes.forEach(id => {
      params = params.append('programmes', id.toString());
    });
    return this.http
      .get<MembershipSemester[]>(`${environment.apiUrl}/Semesters`, { params })
      .pipe(map(a => a.map(e => Object.assign(new MembershipSemester(), e))));
  }
  getAllYears(
    institutions: number[],
    departments: number[],
    programmes: number[]
  ): Observable<MembershipSemester[]> {
    let params = new HttpParams()
      .set('pagination.PageNumber', '0')
      .set('pagination.PageSize', '100');
    institutions.forEach(id => {
      params = params.append('institutions', id.toString());
    });
    departments.forEach(id => {
      params = params.append('departments', id.toString());
    });
    programmes.forEach(id => {
      params = params.append('programmes', id.toString());
    });
    return this.http
      .get<MembershipSemester[]>(`${environment.apiUrl}/Semesters/Years`, { params })
      .pipe(map(a => a.map(e => Object.assign(new MembershipSemester(), e))));
  }
  getOne(id: number): Observable<MembershipSemester> {
    return this.http
      .get<MembershipSemester>(`${environment.apiUrl}/Semesters/${id}`)
      .pipe(map(e => Object.assign(new MembershipSemester(), e)));
  }
}
