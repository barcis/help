import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Programme } from 'src/app/models/U10/Programme';
@Injectable({
  providedIn: 'root'
})
export class ProgrammeService {
  constructor(private http: HttpClient) { }
  getAll(
    institutions: number[],
    departments: number[]
  ): Observable<Programme[]> {
    let params = new HttpParams()
      .set('pagination.PageNumber', '0')
      .set('pagination.PageSize', '100');
    institutions.forEach(id => {
      params = params.append('institutions', id.toString());
    });
    departments.forEach(id => {
      params = params.append('departments', id.toString());
    });
    return this.http
      .get<Programme[]>(`${environment.apiUrl}/Programmes`, { params })
      .pipe(map(a => a.map(e => Object.assign(new Programme(), e))));
  }
  getOne(id: number): Observable<Programme> {
    return this.http
      .get<Programme>(`${environment.apiUrl}/Programmes/${id}`)
      .pipe(map(e => Object.assign(new Programme(), e)));
  }
}
