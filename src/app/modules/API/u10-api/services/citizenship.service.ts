import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { HttpCacheService } from 'src/app/services/httpcache.service';
import { Citizenship } from 'src/app/models/U10/Citizenship';
@Injectable({
  providedIn: 'root'
})
export class CitizenshipService {
  constructor(private http: HttpClient, private cache: HttpCacheService) { }
  getAll(): Observable<Citizenship[]> {
    const params = new HttpParams()
      .set('pagination.PageNumber', '0')
      .set('pagination.PageSize', '100');
    const cacheId = '/Citizenships?' + params.toString();
    const _data = this.cache.get(cacheId);
    if (_data) {
      return of(_data);
    }
    return this.http
      .get<Citizenship[]>(`${environment.apiUrl}/Citizenships`, {
        params
      })
      .pipe(map(a => {
        const data$ = a.map(e => Object.assign(new Citizenship(), e));
        this.cache.put(cacheId, data$);
        return data$;
      }));
  }
}
