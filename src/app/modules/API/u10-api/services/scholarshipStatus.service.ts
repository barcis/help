import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { ScholarshipStatus } from 'src/app/models/U10/ScholarshipStatus';
import { HttpCacheService } from 'src/app/services/httpcache.service';
@Injectable({
  providedIn: 'root'
})
export class ScholarshipStatusService {
  constructor(private http: HttpClient, private cache: HttpCacheService) { }
  getAll(): Observable<ScholarshipStatus[]> {
    const params = new HttpParams()
      .set('pagination.PageNumber', '0')
      .set('pagination.PageSize', '100');


    const cacheId = '/ScholarshipStatuses?' + params.toString();
    const _data = this.cache.get(cacheId);

    if (_data) {
      return of(_data);
    }

    const observable$ = this.http
      .get<ScholarshipStatus[]>(`${environment.apiUrl}/ScholarshipStatuses`, {
        params
      })
      .pipe(map(a => {
        const data$ = a.map(e => Object.assign(new ScholarshipStatus(), e));
        this.cache.put(cacheId, data$);
        return data$;
      }
      ));

    return observable$;
  }
}
