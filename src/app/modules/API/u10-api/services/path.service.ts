import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Department } from 'src/app/models/Department';
import { Programme } from 'src/app/models/Programme';
@Injectable({
  providedIn: 'root'
})
export class PathService {
  constructor(private http: HttpClient) {}
  getInstitutionDepartments(institutionsId: number): Observable<Department[]> {
    return this.http
      .get<Department[]>(
        `${environment.apiUrl}/Institutions/${institutionsId}/Departments`
      )
      .pipe(map(a => a.map(e => Object.assign(new Department(), e))));
  }
  getDepartmentProgrammes(
    institutionsId: number,
    departmentId: number
  ): Observable<Programme[]> {
    return this.http
      .get<Programme[]>(
        `${environment.apiUrl}/Institutions/${institutionsId}/Departments/${departmentId}/Progammes`
      )
      .pipe(map(a => a.map(e => Object.assign(new Programme(), e))));
  }
}
