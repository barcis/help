import { Student } from 'src/app/models/U10/Student';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Operation } from 'fast-json-patch';
import { plainToClass } from 'class-transformer';
import { Person } from 'src/app/models/U10/Person';

@Injectable({
  providedIn: 'root'
})
export class StudentService {
  constructor(private http: HttpClient) { }
  getOneBySLF(slf: string): Observable<Student> {
    return this.http
      .get<Student>(`${environment.apiUrl}/Students/uni10id/${slf}`)
      .pipe(map(e => plainToClass(Student, e)));
  }

  createOne(name: string, surname: string, email: string, uni10Id: string) {
    const _student = new Student();
    _student.person = new Person();
    _student.person.name = name;
    _student.person.surname = surname;
    _student.person.email = email;
    _student.uni10Id = uni10Id;
    return this.http
      .post<Student>(`${environment.apiUrl}/Students/`, _student)
      .pipe(map(e => plainToClass(Student, e)));
  }

  getAll(
    sortColumn: string,
    sortDirection: string,
    pageNumber: number,
    pageSize: number
  ): Observable<Student[]> {
    const params = new HttpParams()
      .set('sort.Column', sortColumn)
      .set('sort.Direction', sortDirection)
      .set('pagination.PageNumber', pageNumber.toString())
      .set('pagination.PageSize', pageSize.toString());
    return this.http
      .get<Student[]>(`${environment.apiUrl}/Students`, { params })
      .pipe(map(a => a.map(e => plainToClass(Student, e))));
  }

  update(id: number, student: Student): Observable<Student> {
    return this.http
      .post<Student>(`${environment.apiUrl}/Students/${id}`, student)
      .pipe(map(e => Object.assign(new Student(), e)));
  }
  patch(id: number, patches: Operation[]) {
    return this.http
      .patch<Student>(`${environment.apiUrl}/Students/${id}`, patches)
      .pipe(map(e => Object.assign(new Student(), e)));
  }
}

