import { DetailsComponent } from './../details/details.component';
import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { merge, of as observableOf } from 'rxjs';
import { startWith, switchMap, map, catchError, first } from 'rxjs/operators';
import { compare } from 'fast-json-patch';
import { JsonPatchBuilder } from '../../class/jsonPatch.builder';
import { classToClass } from 'class-transformer';

import { Student } from 'src/app/models/U10/Student';

import { CommentComponent } from '../comment/comment.component';
import { StudentService } from 'src/app/modules/API/u10-api/services/student.service';
import { PortOfTravelService } from 'src/app/modules/API/u10-api/services/portOfTravel.service';
import { EnrollmentStatusService } from 'src/app/modules/API/u10-api/services/enrollmentStatus.service';
import { ResidentialStatusService } from 'src/app/modules/API/u10-api/services/residentialStatus.service';
import { ScholarshipStatusService } from 'src/app/modules/API/u10-api/services/scholarshipStatus.service';

@Component({
  selector: 'app-register-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements AfterViewInit, OnInit {
  displayedColumns: string[] = [
    'uni10Id',
    'surname',
    'name',
    'gender',
    'portOfTravel',
    'programme',
    'year',
    'semester',
    'gpa',
    'enrollmentStatus',
    'residentialStatus',
    'scholarshipStatus',
    'comment'
  ];
  spans = [];

  students: Student[];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;
  pagination = {
    pageSize: 10
  };

  dictionary = {
    portsOfTravel: [],
    enrollmentStatuses: [],
    residentialStatuses: [],
    scholarshipStatuses: [],
    otherStatuses: []
  };


  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(
    private studentService: StudentService,
    private portOfTravelService: PortOfTravelService,
    private enrollmentStatusService: EnrollmentStatusService,
    private residentialStatusService: ResidentialStatusService,
    private scholarshipStatusService: ScholarshipStatusService,
    private dialog: MatDialog
  ) { }
  ngAfterViewInit() {
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.studentService.getAll(
            this.sort.active,
            this.sort.direction,
            this.paginator.pageIndex,
            this.paginator.pageSize
          );
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.length;

          return data;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the GitHub API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return observableOf([] as Student[]);
        })
      )
      .subscribe(data => {
        const spans = [];
        this.students = data.reduce((current, next, idx) => {
          const rows = next.memberships.length;

          spans.push(rows);

          for (let j = 1; j < rows; j++) {
            spans.push(-1);
          }

          next.memberships.forEach((membership, index) => {
            const student = classToClass(next);
            student.membershipIndex = index;
            current.push(student);
          });
          return current;
        }, []);


        this.spans = spans;
      });
  }

  ngOnInit(): void {
    this.loadPortsOfTravel();
    this.loadEnrollmentStatuses();
    this.loadResidentialStatuses();
    this.loadScholarshipStatuses();
  }

  private loadPortsOfTravel(): void {
    this.dictionary.portsOfTravel = [];

    this.portOfTravelService
      .getAll()
      .pipe(first())
      .subscribe(portsOfTravel => {
        this.dictionary.portsOfTravel = portsOfTravel;
      });
  }

  private loadEnrollmentStatuses(): void {
    this.dictionary.enrollmentStatuses = [];

    this.enrollmentStatusService
      .getAll()
      .pipe(first())
      .subscribe(enrollmentStatuses => {
        this.dictionary.enrollmentStatuses = enrollmentStatuses;
      });
  }

  private loadResidentialStatuses(): void {
    this.dictionary.residentialStatuses = [];

    this.residentialStatusService
      .getAll()
      .pipe(first())
      .subscribe(residentialStatuses => {
        this.dictionary.residentialStatuses = residentialStatuses;
      });
  }
  private loadScholarshipStatuses(): void {
    this.dictionary.scholarshipStatuses = [];

    this.scholarshipStatusService
      .getAll()
      .pipe(first())
      .subscribe(scholarshipStatuses => {
        this.dictionary.scholarshipStatuses = scholarshipStatuses;
      });
  }

  private patchProperty(id: number, name: string, value: any) {
    const jsonPatch = new JsonPatchBuilder();

    this.studentService
      .patch(id, jsonPatch.replace(name, value).generate())
      .subscribe();
  }

  updatePortOfTravel(student: Student): void {
    this.patchProperty(student.id, 'PortOfTravel', student.portOfTravel);
  }

  updateEnrollmentStatus(student: Student): void {
    this.patchProperty(
      student.id,
      'Memberships/' + student.membershipIndex + '/Semester/EnrollmentStatus/Id',
      student.membership.membershipSemester.enrollmentStatus.id
    );
  }
  updateResidentialStatus(student: Student): void {
    this.patchProperty(
      student.id,
      'ResidentialStatus',
      student.residentialStatus
    );
  }
  updateScholarshipStatus(student: Student): void {
    this.patchProperty(
      student.id,
      'ScholarshipStatus',
      student.scholarshipStatus
    );
  }
  updateOtherStatus(student: Student): void {
    this.patchProperty(
      student.id,
      'OtherStatus',
      student.scholarshipStatus
    );
  }

  openCommentDialog(student: Student): void {
    const studentCopy = classToClass(student);

    const dialogRef = this.dialog.open(CommentComponent, {
      maxWidth: '100%',
      width: '550px',
      data: studentCopy
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const flatStudent = JSON.parse(JSON.stringify(student));
        const flatResult = JSON.parse(JSON.stringify(result));

        const patch = compare(flatStudent, flatResult);
        Object.assign(student, result);
        if (patch.length > 0) {
          this.studentService
            .patch(student.id, patch)
            .subscribe();
        }
      }
    });
  }

  openDetailsDialog(student: Student): void {
    const studentCopy = classToClass(student);
    const dialogRef = this.dialog.open(DetailsComponent, {
      maxWidth: '90%',
      width: 'auto',
      minWidth: '75%',
      data: studentCopy
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const flatStudent = JSON.parse(JSON.stringify(student));
        const flatResult = JSON.parse(JSON.stringify(result));

        const patch = compare(flatStudent, flatResult);
        Object.assign(student, result);
        if (patch.length > 0) {
          this.studentService
            .patch(student.id, patch)
            .subscribe();
        }
      }
    });
  }
}
