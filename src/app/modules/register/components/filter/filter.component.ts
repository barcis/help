import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { Institution } from 'src/app/models/U10/Institution';
import { Department } from 'src/app/models/U10/Department';
import { Programme } from 'src/app/models/U10/Programme';
import { MembershipSemester } from 'src/app/models/U10/MembershipSemester';
import { Filters } from '../../class/filters';
import { InstitutionService } from 'src/app/modules/API/u10-api/services/institution.service';
import { DepartmentService } from 'src/app/modules/API/u10-api/services/department.service';
import { SemesterService } from 'src/app/modules/API/u10-api/services/semester.service';
import { ProgrammeService } from 'src/app/modules/API/u10-api/services/programme.service';

@Component({
  selector: 'app-register-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
  selected: Filters = new Filters();
  institutions: Institution[] = [];
  departments: Department[] = [];
  programmes: Programme[] = [];
  semesters: MembershipSemester[] = [];
  loading = 0;
  constructor(
    private institutionService: InstitutionService,
    private departmentService: DepartmentService,
    private semesterService: SemesterService,
    private programmeService: ProgrammeService
  ) { }

  public refreshLists(): void {
    this.loadDepartments();
    this.loadProgrammes();
    // this.loadSemesters();
  }

  private loadInstitutions(): void {
    if (this.institutions.length === 0) {
      this.loading++;
      this.institutionService
        .getAll()
        .pipe(first())
        .subscribe(institutions => {
          this.loading--;
          this.institutions = institutions;
        });
    }
  }

  private loadDepartments(): void {
    if (this.selected.institutions.length > 0) {
      this.loading++;
      this.departments = [];

      this.departmentService
        .getAll(this.selected.institutions)
        .pipe(first())
        .subscribe(departments => {
          this.loading--;
          this.departments = departments;
        });
    }
  }

  private loadProgrammes(): void {
    if (this.selected.institutions.length > 0) {
      this.loading++;
      this.programmes = [];

      this.programmeService
        .getAll(this.selected.institutions, this.selected.departments)
        .pipe(first())
        .subscribe(programmes => {
          this.loading--;
          this.programmes = programmes;
        });
    }
  }

  private loadSemesters(): void {
    if (this.selected.institutions.length > 0) {
      this.loading++;
      this.semesters = [];

      this.semesterService
        .getAllYears(
          this.selected.institutions,
          this.selected.departments,
          this.selected.programmes
        )
        .pipe(first())
        .subscribe(semesters => {
          this.loading--;
          this.semesters = semesters;
        });
    }
  }

  ngOnInit(): void {
    this.loadInstitutions();
  }
}
