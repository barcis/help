import { DetalsDefinitionGroup, DetalsDefinitionGroupXML } from './../../class/DetalsDefinitionGroup';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Student } from 'src/app/models/U10/Student';
import { first, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ConfigService } from 'src/app/services/config.service';
import { DetalsDefinition } from '../../class/DetalsDefinition';
import { PredefinedFields } from '../../class/PredefinedFields';
import { DetalsDefinitionTab } from '../../class/DetalsDefinitionTab';
import { DetalsDefinitionField } from '../../class/DetalsDefinitionField';
import { PortOfTravelService } from 'src/app/modules/API/u10-api/services/portOfTravel.service';
import { EnrollmentStatusService } from 'src/app/modules/API/u10-api/services/enrollmentStatus.service';
import { ResidentialStatusService } from 'src/app/modules/API/u10-api/services/residentialStatus.service';
import { ScholarshipStatusService } from 'src/app/modules/API/u10-api/services/scholarshipStatus.service';
import { CitizenshipService } from 'src/app/modules/API/u10-api/services/citizenship.service';
import { AdditionalService } from 'src/app/modules/API/u10-api/services/additional.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  detalsDefinition: DetalsDefinition = new DetalsDefinition();
  predefinedFields: PredefinedFields = new PredefinedFields();

  constructor(
    public dialogRef: MatDialogRef<DetailsComponent>,
    private portOfTravelService: PortOfTravelService,
    private enrollmentStatusService: EnrollmentStatusService,
    private residentialStatusService: ResidentialStatusService,
    private scholarshipStatusService: ScholarshipStatusService,
    private citizenshipService: CitizenshipService,
    private additionalService: AdditionalService,
    private configService: ConfigService,
    @Inject(MAT_DIALOG_DATA) public student: Student) {
    this.detalsDefinition.tabs = [];
  }
  public ngOnInit() {
    const problem = this.predefinedFields['_problem'];

    this.configService
      .getConfigXML()
      .pipe(first())
      .subscribe(fromXML => {
        fromXML.then(ddXML => {
          ddXML.tabs.forEach(tab => {
            const ddTab = new DetalsDefinitionTab();
            this.detalsDefinition.tabs.push(ddTab);

            ddTab.groups = [];
            ddTab.label = tab.label;
            ddTab.columns = tab.columns;

            if (!tab.groups) {
              tab.groups = [];
            }

            if (tab.fields) {
              const defaultGroup = new DetalsDefinitionGroupXML();
              tab.groups.push(defaultGroup);

              defaultGroup.fields = tab.fields;
              defaultGroup.columns = tab.columns;
              ddTab.columns = 1;
            }

            tab.groups.forEach(group => {
              const ddGroup = new DetalsDefinitionGroup();
              ddTab.groups.push(ddGroup);
              ddGroup.fields = [];
              ddGroup.label = group.label;
              ddGroup.columns = group.columns;
              ddGroup.span = group.span;

              group.fields.forEach(field => {
                let ddField: DetalsDefinitionField;
                let baseField: DetalsDefinitionField;

                if (field.type === 'studentAdditional') {
                  baseField = new DetalsDefinitionField();
                  baseField.label = field.label;
                  baseField.type = 'text';
                  baseField.source = 'student.additional.id_' + field.typeId + '';

                } else if (field.type === 'membershipAdditional') {
                  baseField = new DetalsDefinitionField();
                  baseField.label = field.label;
                  baseField.type = 'text';
                  baseField.source = 'student.membership.additional.id_' + field.typeId + '';
                } else {
                  baseField = this.predefinedFields[field.type] as DetalsDefinitionField;
                }

                if (baseField) {
                  ddField = Object.assign(new DetalsDefinitionField(), baseField);
                  if (typeof (baseField.editable) !== 'undefined' && baseField.editable !== null) {
                    ddField.editable = baseField.editable;
                  } else {
                    ddField.editable = true;
                  }
                  if (field.label) {
                    ddField.label = field.label;
                  }
                  if (field.placeholder) {
                    ddField.placeholder = field.placeholder;
                  }

                  ddField.span = field.span;
                  if ((baseField.type === 'select' || baseField.type === 'multiple') && baseField.itemsSource) {
                    if (typeof (this[baseField.itemsSource]) === 'function') {
                      (this[baseField.itemsSource]() as Observable<any>).subscribe(items => {
                        ddField.items = items;
                      });
                    }
                  }

                } else {
                  ddField = Object.assign(new DetalsDefinitionField(), problem);
                  ddField.label += field.type;
                }
                if (field.type === 'studentAdditional') {
                  this.loadStudentAdditional(field.typeId, ddField);
                }
                ddGroup.fields.push(ddField);
              });

            });

          });
        });
      });
  }


  public compareById(o1: any, o2: any): boolean {
    return o1.id === o2.id;
  }

  private loadStudentAdditional(typeId: number, field: DetalsDefinitionField) {
    this.additionalService
      .getOneForStudent(this.student.id, typeId)
      .pipe(first())
      .subscribe(result => {
        this.student.additional['id_' + typeId] = result.value;
        switch (result.type.edit) {
          case 2:
            field.type = 'select';
            if (result.type.options) {
              field.items = result.type.options.map(item => ({ id: item.value, value: item.value }));
            }
            break;

          default:
            break;

        }
      });
  }

  private loadCitizenships(): Observable<any> {
    return this.citizenshipService
      .getAll()
      .pipe(first());
  }

  private loadPortsOfTravel(): Observable<any> {
    return this.portOfTravelService
      .getAll()
      .pipe(first())
      .pipe(
        map(items => items.map(item => ({ id: item.name, value: item.name })))
      );
  }

  private loadEnrollmentStatuses(): Observable<any> {
    return this.enrollmentStatusService
      .getAll()
      .pipe(first())
      .pipe(
        map(items => items.map(item => ({ id: item.name, value: item.name })))
      );
  }

  private loadResidentialStatuses(): Observable<any> {
    return this.residentialStatusService
      .getAll()
      .pipe(first())
      .pipe(
        map(items => items.map(item => ({ id: item.name, value: item.name })))
      );
  }
  private loadScholarshipStatuses(): Observable<any> {
    return this.scholarshipStatusService
      .getAll()
      .pipe(first())
      .pipe(
        map(items => items.map(item => ({ id: item.name, value: item.name })))
      );
  }

  source(name: string, value: any): any {

    if (!name) {
      return;
    }

    let o = this;
    name = name.replace(/\[(\w+)\]/g, '.$1');
    name = name.replace(/^\./, '');
    const parts = name.split('.');

    let k: string;
    let obj: any;
    for (let i = 0, n = parts.length; i < n; ++i) {
      k = parts[i];
      if (k in o) {
        obj = o;
        o = o[k];
      } else {
        return;
      }
    }
    if (arguments.length === 2) {
      obj[k] = value;
    }

    return obj[k];
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
