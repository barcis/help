import { DetalsDefinitionFieldDictionary } from './DetalsDefinitionFieldDictionary';
export class DetalsDefinitionField {
  label?: string;
  type: string;
  placeholder?: string;
  source?: string;
  itemsSource?: string;
  editable?: boolean;
  items?: Array<DetalsDefinitionFieldDictionary>;
  span: number;
  getSpan(): number {
    return this.span ? this.span : 1;
  }
}

export class DetalsDefinitionFieldXML {
  label?: string;
  type: string;
  typeId: number;
  placeholder?: string;
  source?: string;
  itemsSource?: string;
  editable?: boolean;
  items?: Array<DetalsDefinitionFieldDictionary>;
  span?: number;
  getSpan(): number {
    return this.span ? this.span : 1;
  }
}
