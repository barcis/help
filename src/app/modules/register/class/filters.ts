export class Filters {
  institutions: number[] = [];
  departments: number[] = [];
  programmes: number[] = [];
  years: number[] = [];
  text = '';
}
