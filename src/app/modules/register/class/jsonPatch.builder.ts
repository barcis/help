import { Operation, AddOperation, ReplaceOperation, RemoveOperation } from 'fast-json-patch';

export class JsonPatchBuilder {
  private patches: Operation[] = [];
  replace(path: string, value: any): JsonPatchBuilder {
    const t: ReplaceOperation<any> = { op: 'replace', path: '/' + path, value };
    this.patches.push(t);
    return this;
  }
  remove(path: string): JsonPatchBuilder {
    const t: RemoveOperation = { op: 'remove', path: '/' + path };
    this.patches.push(t);
    return this;
  }
  add(path: string, value: any): JsonPatchBuilder {
    const t: AddOperation<any> = { op: 'add', path: '/' + path, value };
    this.patches.push(t);
    return this;
  }

  generate(): Operation[] {
    return this.patches;
  }
}
