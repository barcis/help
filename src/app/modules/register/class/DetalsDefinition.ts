import { DetalsDefinitionTab, DetalsDefinitionTabXML } from './DetalsDefinitionTab';
export class DetalsDefinition {
  tabs: Array<DetalsDefinitionTab>;
}
export class DetalsDefinitionXML {
  tabs: Array<DetalsDefinitionTabXML>;
}
