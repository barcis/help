export class JsonPatch {
  op: string;
  path: string;
  from: string;
  value: string;
}
