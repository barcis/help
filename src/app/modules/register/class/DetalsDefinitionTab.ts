import { DetalsDefinitionFieldXML } from './DetalsDefinitionField';
import { DetalsDefinitionGroup, DetalsDefinitionGroupXML } from './DetalsDefinitionGroup';
export class DetalsDefinitionTab {
  label: string;
  groups: Array<DetalsDefinitionGroup>;
  columns: number;
  getColumns(): number {
    return this.columns ? this.columns : 1;
  }
}

export class DetalsDefinitionTabXML {
  label: string;
  fields: Array<DetalsDefinitionFieldXML>;
  groups: Array<DetalsDefinitionGroupXML>;
  columns: number;
  getColumns(): number {
    return this.columns ? this.columns : 1;
  }
}

