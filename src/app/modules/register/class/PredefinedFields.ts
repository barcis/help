import { DetalsDefinitionField } from './DetalsDefinitionField';
export class PredefinedFields {
  // from student
  studentUni10Id: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'Uni10Id', source: 'student.uni10Id', type: 'text', placeholder: 'Student Id in U10 system' });
  studentAllocator: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Allocator', source: 'student.allocator', type: 'select', placeholder: 'Student allocator name',
    items: [
      { id: 'Allocator1', value: 'Allocator1' },
      { id: 'Allocator2', value: 'Allocator2' }
    ]
  });
  studentComment: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'Comment', source: 'student.comment', type: 'textarea', placeholder: 'Ex. Good student' });
  studentPortOfTravel: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'Port of travel', source: 'student.portOfTravel', type: 'select', placeholder: 'Student port of travel', itemsSource: 'loadPortsOfTravel' });
  studentResidentialStatus: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Residential status',
    source: 'student.residentialStatus', type: 'select', placeholder: 'Student residential status', itemsSource: 'loadResidentialStatuses'
  });
  studentScholarshipStatus: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Scholarship status',
    source: 'student.scholarshipStatus', type: 'select', placeholder: 'Student scholarship status', itemsSource: 'loadScholarshipStatuses'
  });
  studentOtherStatus: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'Other status', source: 'student.otherStatus', type: 'select', placeholder: 'Student other status', itemsSource: 'loadOtherStatuses' });
  // from membership
  membershipId: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'Id', source: 'student.membership.id', type: 'text', editable: false, placeholder: 'Membership id' });
  membershipAlbumNumber: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'Album number', source: 'student.membership.albumNumber', type: 'text', placeholder: 'Student album number' });
  membershipSpecialty: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Specialty',
    source: 'student.membership.module.specialty.name', type: 'text', placeholder: 'Student specialty', editable: false
  });
  membershipSpecialization: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Specialization', source: 'student.membership.module.specialization.name',
    type: 'text', placeholder: 'Student specialization', editable: false
  });
  // tslint:disable-next-line: max-line-length
  membershipKind: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'Kind', source: 'student.membership.kind.name', type: 'text', editable: false, placeholder: 'Kind' });
  membershipProgramme: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'Programme', source: 'student.membership.programme.name', type: 'text', editable: false, placeholder: 'Programme' });
  membershipDepartment: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'Department', source: 'student.membership.department.name', type: 'text', editable: false, placeholder: 'Department' });
  membershipInstitution: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'Institution', source: 'student.membership.institution.name', type: 'text', editable: false, placeholder: 'Institution' });
  membershipYearOfStudy: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Year Of Study', source: 'student.membership.membershipSemester.semester.yearOfStudy', type: 'text', editable: false,
    placeholder: 'Student year of study'
  });
  // from person
  // tslint:disable-next-line: max-line-length
  personId: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'Id', source: 'student.person.id', type: 'text', placeholder: 'Student person id', editable: false });
  personSubId: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'SubId', source: 'student.person.subId', type: 'text', placeholder: 'Student subject id', editable: false });
  personGender: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Gender', source: 'student.person.gender', type: 'select', placeholder: 'Student first name',
    items: [
      { id: 1, value: 'Female' },
      { id: 2, value: 'Male' }
    ]
  });
  personName: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'Name', source: 'student.person.name', type: 'text', placeholder: 'Student first name' });
  // tslint:disable-next-line: max-line-length
  personSurname: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'Surname', source: 'student.person.surname', type: 'text', placeholder: 'Student last name' });
  personEmail: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'Email', source: 'student.person.email', type: 'email', placeholder: 'Student email' });
  // tslint:disable-next-line: max-line-length
  personEmail2: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'Second email', source: 'student.person.email2', type: 'email', placeholder: 'Student second email' });
  personEmailWork: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'Email (work)', source: 'student.person.emailwork', type: 'email', placeholder: 'Student job email' });
  personPhoneNumber: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'Phone number', source: 'student.person.phoneNumber', type: 'text', placeholder: 'Student phone number' });
  personPhoneNumberHome: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Phone number (home)', source: 'student.person.phoneNumberHome', type: 'text', placeholder: 'Student phone number to home'
  });
  personPhoneNumberWork: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Phone number (work)', source: 'student.person.phoneNumberWork', type: 'text', placeholder: 'Student phone number to work'
  });
  personDateOfBirth: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'Date of birth', source: 'student.person.dateOfBirth', type: 'date', placeholder: 'Student date of birth' });
  personPlaceOfBirth: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'Place of birth', source: 'student.person.placeOfBirth', type: 'text', placeholder: 'Student place of birth' });
  // TODO: dopisac obsługę po stronie C#
  personCitizenship: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Citizenships', source: 'student.person.citizenships', type: 'multiple', placeholder: 'Student citizenships', itemsSource: 'loadCitizenships'
  });
  personNationality: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'Nationality', source: 'student.person.nationality', type: 'text', placeholder: 'Student nationality' });
  personOrigin: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), { label: 'Origin', source: 'student.person.origin', type: 'text', placeholder: 'Student origin' });
  personCompany: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Company', source: 'student.person.company', type: 'text', placeholder: 'Student company'
  });
  personMaritalStatus: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Marital status', source: 'student.person.maritalStatus', type: 'select', placeholder: 'Student marital status',
    items: [
      { id: 0, value: 'unknown' },
      { id: 1, value: 'unmarried woman' },
      { id: 2, value: 'unmarried man' },
      { id: 3, value: 'married woman' },
      { id: 4, value: 'married man' },
      { id: 5, value: 'widow' },
      { id: 6, value: 'widower' },
    ]
  });
  // from person address
  addressStreet: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Street', source: 'student.person.address.street', type: 'text', placeholder: 'street'
  });
  addressHomeNumber: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Home number', source: 'student.person.address.homeNumber', type: 'text', placeholder: 'home number'
  });
  addressFlatNumber: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Flat number', source: 'student.person.address.flatNumber', type: 'text', placeholder: 'flat number'
  });
  addressPostOffice: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Post Office', source: 'student.person.address.postOffice', type: 'text', placeholder: 'post office'
  });
  addressPostalCode: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Postal Code', source: 'student.person.address.postalCode', type: 'text', placeholder: 'postal code'
  });
  addressTown: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Town', source: 'student.person.address.town', type: 'text', placeholder: 'town'
  });
  addressProvince: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Province', source: 'student.person.address.province', type: 'text', placeholder: 'province'
  });
  addressDistrict: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'District', source: 'student.person.address.district', type: 'text', placeholder: 'district'
  });
  addressLocalGovernment: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Local Government', source: 'student.person.address.localGovernment', type: 'text', placeholder: 'local government'
  });



  // from person correspondence address
  correspondenceAddressStreet: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Street', source: 'student.person.correspondenceAddress.street', type: 'text', placeholder: 'street'
  });
  correspondenceAddressHomeNumber: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Home number', source: 'student.person.correspondenceAddress.homeNumber', type: 'text', placeholder: 'home number'
  });
  correspondenceAddressFlatNumber: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Flat number', source: 'student.person.correspondenceAddress.flatNumber', type: 'text', placeholder: 'flat number'
  });
  correspondenceAddressPostOffice: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Post Office', source: 'student.person.correspondenceAddress.postOffice', type: 'text', placeholder: 'post office'
  });
  correspondenceAddressPostalCode: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Postal Code', source: 'student.person.correspondenceAddress.postalCode', type: 'text', placeholder: 'postal code'
  });
  correspondenceAddressTown: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Town', source: 'student.person.correspondenceAddress.town', type: 'text', placeholder: 'town'
  });
  correspondenceAddressProvince: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Province', source: 'student.person.correspondenceAddress.province', type: 'text', placeholder: 'province'
  });
  correspondenceAddressDistrict: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'District', source: 'student.person.correspondenceAddress.district', type: 'text', placeholder: 'district'
  });
  correspondenceAddressLocalGovernment: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    label: 'Local Government', source: 'student.person.correspondenceAddress.localGovernment', type: 'text', placeholder: 'local government'
  });
  // technical
  _problem: DetalsDefinitionField = Object.assign(new DetalsDefinitionField(), {
    type: 'label',
    label: 'Error with field: '
  });
}
