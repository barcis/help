import { DetalsDefinitionField, DetalsDefinitionFieldXML } from './DetalsDefinitionField';
export class DetalsDefinitionGroup {
  label: string;
  fields: Array<DetalsDefinitionField>;
  columns: number;
  span: number;
  getSpan(): number {
    return this.span ? this.span : 1;
  }
  getColumns(): number {
    return this.columns ? this.columns : 1;
  }
}

export class DetalsDefinitionGroupXML {
  label: string;
  fields: Array<DetalsDefinitionFieldXML>;
  columns: number;
  span: number;
  getSpan(): number {
    return this.span ? this.span : 1;
  }
  getColumns(): number {
    return this.columns ? this.columns : 1;
  }
}
