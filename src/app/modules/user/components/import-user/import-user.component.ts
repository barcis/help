import { Student } from 'src/app/models/U10/Student';
import { Component, ViewChild } from '@angular/core';
import { StudentService } from 'src/app/modules/API/u10-api/services/student.service';
import { UserService } from 'src/app/modules/API/idp-api/services/user.service';
import { JsonPatchBuilder } from 'src/app/modules/register/class/jsonPatch.builder';

export class CSVRecord {
  public slf: string;
  public name: string;
  public surname: string;
  public password: string;
  public id: string;
  public subid: string;

  public get email() {
    return this.name.toLocaleLowerCase() + '.' + this.surname.toLocaleLowerCase() + '@pcg.com';
  }
}

@Component({
  selector: 'app-import-user',
  templateUrl: './import-user.component.html',
  styleUrls: ['./import-user.component.css']
})
export class ImportUserComponent {

  progressValue = 0;
  bufferValue = 0;
  public records: CSVRecord[] = [];
  @ViewChild('csvReader') csvReader: any;
  displayedColumns: string[] = ['position', 'slf', 'name', 'surname', 'email', 'password', 'id', 'subid', 'actions'];

  constructor(
    private studentService: StudentService,
    private userService: UserService
  ) { }

  uploadListener($event: any): void {

    const files = $event.srcElement.files;

    if (this.isValidCSVFile(files[0])) {

      const input = $event.target;
      const reader = new FileReader();
      reader.readAsText(input.files[0]);

      reader.onload = () => {
        const csvRecordsArray = (<string>reader.result).split(/\r\n|\n/);

        const headersRow = this.getHeaderArray(csvRecordsArray);

        this.records = this.getDataRecordsArrayFromCSVFile(csvRecordsArray, headersRow.length);
        console.log('readed ' + this.records.length + ' rows...');
      };

      reader.onerror = function () {
        console.log('error is occured while reading file!');
      };

    } else {
      alert('Please import valid .csv file.');
      this.fileReset();
    }
  }

  getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {
    const csvArr = [];

    this.bufferValue = 0;

    const recordsLength = csvRecordsArray.length;

    for (let i = 1; i < recordsLength; i++) {

      this.bufferValue = Math.ceil(i / recordsLength * 100);

      const curruntRecord = (<string>csvRecordsArray[i]).split(';');
      if (curruntRecord.length === headerLength) {
        const csvRecord: CSVRecord = new CSVRecord();
        csvRecord.slf = curruntRecord[0].trim();
        csvRecord.name = curruntRecord[1].trim();
        csvRecord.surname = curruntRecord[2].trim();
        csvRecord.password = curruntRecord[3].trim();
        csvArr.push(csvRecord);
      }
    }
    return csvArr;
  }

  isValidCSVFile(file: any) {
    return file.name.endsWith('.csv');
  }

  getHeaderArray(csvRecordsArr: any) {
    const headers = (<string>csvRecordsArr[0]).split(';');
    const headerArray = [];
    for (let j = 0; j < headers.length; j++) {
      headerArray.push(headers[j]);
    }
    return headerArray;
  }

  fileReset() {
    this.csvReader.nativeElement.value = '';
    this.records = [];
  }

  get limitedRecords() {
    return this.records.slice(0, 10);
  }

  findOrCreateInU10(student: CSVRecord) {
    this.studentService.getOneBySLF(student.slf).subscribe(
      (s: Student) => {
        student.id = s.id.toString();
        if (s.person.subId) {
          student.subid = s.person.subId;
        }
      },
      (error) => {
        this.studentService.createOne(student.name, student.surname, student.email, student.slf).subscribe((s: Student) => {
          student.id = s.id.toString();
          if (s.person.subId) {
            student.subid = s.person.subId;
          }
        });
      }
    );
  }

  createInIdP(student: CSVRecord) {
    this.userService.createOne(student.name, student.surname, student.email, student.slf).subscribe(s => {
      student.subid = s.subjectId;

      const jsonPatch = new JsonPatchBuilder();

      this.studentService
        .patch(
          parseInt(student.id),
          jsonPatch.replace('Person/SubId', student.subid).generate())
        .subscribe();
      this.userService.setPassword(s.subjectId, student.password).subscribe(p => console.log('p', p));
      this.userService.addRole(s.subjectId, 2).subscribe(r => console.log('r', r));
    });
  }

}
