import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatProgressBarModule } from '@angular/material/progress-bar';

import { ImportUserComponent } from './components/import-user/import-user.component';
@NgModule({
  declarations: [ImportUserComponent],
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatTableModule,
    MatProgressBarModule,
    HttpClientModule,
    RouterModule.forChild([
      { path: '', component: ImportUserComponent, pathMatch: 'full' }
    ])
  ]
})
export class UserModule {

}
