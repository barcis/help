import { Component, OnInit } from '@angular/core';
import { OidcSecurityService } from 'angular-auth-oidc-client';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {
  isExpanded = false;
  isAuthenticated: boolean;
  userRoles: Array<string>;

  constructor(private oidcSecurityService: OidcSecurityService) { }

  ngOnInit() {
    this.oidcSecurityService.getIsAuthorized().subscribe(auth => {
      this.isAuthenticated = auth;
    });
    this.oidcSecurityService.getUserData().subscribe(userData => {
      this.userRoles = !Array.isArray(userData.role) ? [userData.role] : userData.role;
    });
  }

  UserRolesContains(roles: Array<string>) {
    for (const role of roles) {
      if (this.userRoles.indexOf(role) >= 0) {
        return true;
      }
    }
    return false;
  }

  public login() {
    this.oidcSecurityService.authorize();
  }

  public logout() {
    this.oidcSecurityService.logoff();
  }

  public collapse() {
    this.isExpanded = false;
  }

  public toggle() {
    this.isExpanded = !this.isExpanded;
  }

}
