import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { MatButtonModule } from '@angular/material/button';

import { HomeComponent } from '../base/components/home/home.component';
import { NavMenuComponent } from '../base/components/nav-menu/nav-menu.component';

@NgModule({
  declarations: [HomeComponent, NavMenuComponent],
  imports: [
    // BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    CommonModule,
    FormsModule,
    MatButtonModule,
    HttpClientModule,
    RouterModule.forChild([
      { path: '', component: HomeComponent, pathMatch: 'full' }
    ])
  ],
  exports: [
    NavMenuComponent
  ]
})
export class BaseModule { }
