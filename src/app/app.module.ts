import { environment } from './../environments/environment';
import { NgModule } from '@angular/core';
import { AppRouting } from './app.routing';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { OidcSecurityService, OpenIdConfiguration, AuthWellKnownEndpoints, AuthModule } from 'angular-auth-oidc-client';

import { AppComponent } from './app.component';

import { JwtInterceptor } from './helpers/jwt-interceptor';
import { ErrorInterceptor } from './helpers/error-interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BaseModule } from './modules/base/base.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    AuthModule.forRoot(),
    BrowserAnimationsModule,
    AppRouting,
    BaseModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(public oidcSecurityService: OidcSecurityService) {


    const config: OpenIdConfiguration = {
      stsServer: environment.auth.issuer,
      redirect_url: window.location.origin + environment.appSubdir,
      client_id: environment.auth.clientID,
      response_type: 'code',
      scope: environment.auth.scope,
      post_logout_redirect_uri: window.location.origin + environment.appSubdir,
      start_checksession: true,
      silent_renew: true,
      silent_renew_url: window.location.origin + environment.appSubdir + '/silent-renew.html',
      post_login_route: '/',
      forbidden_route: '/Forbidden',
      unauthorized_route: '/Unauthorized',
      log_console_warning_active: false,
      log_console_debug_active: false,
      max_id_token_iat_offset_allowed_in_seconds: 10,
    };

    const authWellKnownEndpoints: AuthWellKnownEndpoints = {
      issuer: environment.auth.issuer,
      jwks_uri: environment.auth.issuer + '/.well-known/openid-configuration/jwks',
      authorization_endpoint: environment.auth.issuer + '/connect/authorize',
      token_endpoint: environment.auth.issuer + '/connect/token',
      userinfo_endpoint: environment.auth.issuer + '/connect/userinfo',
      end_session_endpoint: environment.auth.issuer + '/connect/endsession',
      check_session_iframe: environment.auth.issuer + '/connect/checksession',
      revocation_endpoint: environment.auth.issuer + '/connect/revocation',
      introspection_endpoint: environment.auth.issuer + '/connect/introspect',
    };

    this.oidcSecurityService.setupModule(config, authWellKnownEndpoints);
  }

}
