import { Component } from '@angular/core';
import { environment } from 'src/environments/environment';
// import {
//   OAuthService,
//   NullValidationHandler,
//   AuthConfig
// } from 'angular-oauth2-oidc';

// const authConfig: AuthConfig = {
//   redirectUri: window.location.origin + environment.appSubdir,
//   clientId: environment.auth.clientID,
//   issuer: environment.auth.issuer,
//   scope: environment.auth.scope,
//   showDebugInformation: true,
//   responseType: 'code',
//   disableAtHashCheck: true
// };

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'app';
  // constructor(private oauthService: OAuthService) {
  //   this.oauthService.configure(authConfig);

  //   this.oauthService.tokenValidationHandler = new NullValidationHandler();
  //   this.oauthService.loadDiscoveryDocumentAndTryLogin();
  //   this.oauthService.setupAutomaticSilentRefresh();
  // }
}
